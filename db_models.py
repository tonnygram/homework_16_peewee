from peewee import Model, IntegerField, TextField, BigIntegerField, DateTimeField
import datetime

from db import sqlite


class BaseModel(Model):
    class Meta:
        database = sqlite


class States(BaseModel):
    data = TextField()
    ts = BigIntegerField()


class Item(BaseModel):
    uid = IntegerField(unique=True)
    title = TextField()
    url = TextField()
    created_date = DateTimeField(default=datetime.datetime.now)


def create_tables():
    sqlite.create_tables([States, Item])
