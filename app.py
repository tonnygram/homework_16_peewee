from flask import Flask
import app_config
from db_models import create_tables
from views import index

app = Flask(__name__)
app.add_url_rule('/', 'index', index, methods=['GET', 'POST'], )
create_tables()

if __name__ == "__main__":
    app.run(host=app_config.host, port=app_config.port, debug=app_config.debug)
