import requests

TOP_STORIES_URL = 'https://hacker-news.firebaseio.com/v0/topstories.json'
ITEM_URL = 'https://hacker-news.firebaseio.com//v0/item/{}.json'


def get_top_stories(limit: int = 10) -> list:
    return requests.get(TOP_STORIES_URL).json()[:limit]


def get_item(id_item: int) -> dict:
    result = requests.get(ITEM_URL.format(id_item)).json()
    title = result['title']
    url = result['url']
    return dict(title=title, url=url)
