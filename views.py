import json

from db_models import States
from time import time
import hacker_news
from flask import render_template, request

__all__ = {'index'}


def index() -> str:
    _get_current_state()
    if request.method == 'POST':
        option_1 = request.form['select1']
        option_2 = request.form['select2']
        return render_template('index.html', is_form_submit=True, options=_get_option(), table=_make_table(option_1,
                                                                                                           option_2))
    else:
        return render_template('index.html', is_form_submit=False, options=_get_option())


def get_position(list_1: list, list_2: list) -> list:
    return [(item, list_1.index(item) - i) if item in list_1 else (item, len(list_1)) for i, item in
            enumerate(list_2)]


def _get_option():
    return [str(s.ts) for s in States.select(States.ts)]


def _make_table(option_1, option_2):
    if option_1 == option_2:
        where_clause = States.ts == option_1
    else:
        where_clause = States.ts in (option_1, option_2)

        selected_states = list(States.select(States.data).where(where_clause))
        if len(selected_states) == 1:
            pass


def _get_current_state():
    last_update = list(States.select(States.ts).order_by(States.ts, 'DESC').limit(1))
    if not last_update or time() - last_update[0].ts > 60:
        States(
            data=json.dumps(hacker_news.get_top_stories()),
            ts=time()
        ).save()
